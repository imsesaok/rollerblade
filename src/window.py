# window.py
#
# Copyright 2023 Yuna
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Adw
from gi.repository import Gio
from gi.repository import Gtk

@Gtk.Template(resource_path='/space/cherryband/rollerblade/window.ui')
class RollerbladeWindow(Adw.ApplicationWindow):
    __gtype_name__ = 'RollerbladeWindow'

    photo_view = Gtk.Template.Child()
    button_open_file = Gtk.Template.Child()

    def __init__(self, **kwargs):
        super().__init__(**kwargs)

        action_open_file = Gio.SimpleAction(name="open")
        action_open_file.connect("activate", self.open_file_dialog)
        self.add_action(action_open_file)

    def open_file_dialog(self, action, _):
        native = Gtk.FileDialog()
        native.open(self, None, self.on_open_file_response)

    def on_open_file_response(self, dialog, result):
        file = dialog.open_finish(result)

        if file is not None:
            self.open_file(file)

    def open_file(self, file):
        file.load_contents_async(None, self.open_file_complete)

    def open_file_complete(self, file, result):
        contents = file.load_contents_finish(result)
        if not contents[0]:
            path = file.peek_path()
            print(f"Unable to open {path}: {contents[1]}")

        self.photo_view.set_file(file)
